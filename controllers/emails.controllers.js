const { validationResult } = require("express-validator");
const Email = require("../model/Email");
const Category = require("../model/Category");

const create = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  const { emailId, category } = req.body;
  const { user } = req;
  try {
    let email = await Email.findOne({
      emailId,
    });
    if (email) {
      return res.status(400).json({
        msg: "Email Already Exists",
      });
    }

    let getCategory = await Category.findOne({ _id: category });
    if (!getCategory) {
      return res.status(422).json({
        msg: "Invalid Category",
      });
    }

    email = new Email({
      emailId,
      category,
      user: user._id,
    });

    await email.save();

    const payload = {
      email,
      success: true
    };
    res.status(200).json(payload);
  } catch (err) {
    console.log(err.message);
    res.status(500).send("Error in Saving");
  }
};

const get = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  const { id } = req.params;
  const { user } = req;
  try {
    let email = await Email.findOne({
      _id: id,
      user: user._id,
    }).populate("category", "name");
    if (!email) {
      return res.status(404).json({
        msg: "Email Not Exists",
      });
    }

    const payload = {
      email,
      success: true
    };
    res.status(200).json(payload);
  } catch (err) {
    console.log(err.message);
    res.status(500).send({msg: "Error while fetching"});
  }
};

const getAll = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  const { user } = req;
  let where = { user: user._id };
  const { categoryId } = req.query;
  if (categoryId) {
    where = { ...where, category: categoryId };
  }

  try {
    let emails = await Email.find(where).populate("category", "name");
    if (!emails) {
      return res.status(404).json({
        msg: "Category Not Exists",
      });
    }

    const payload = {
      emails,
      success: true
    };
    res.status(200).json(payload);
  } catch (err) {
    console.log(err.message);
    res.status(500).send({msg: "Error in Fetching"});
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  const { id } = req.params;
  const { body, user } = req;
  const { category, emailId } = body;
  try {
    let obj = {};

    if (category) {
      let getCategory = await Category.findOne({ _id: category });
      if (!getCategory) {
        return res.status(422).json({
          msg: "Invalid Category",
        });
      }
      obj = {...obj, category};
    }

    if (emailId) {
      obj = {...obj, emailId};
    }

    Email.updateOne(
      {
        _id: id,
        user: user._id,
      },
      obj
    )
      .then((data) => {
        return res.status(200).json({ success: true });
      })
      .catch((er) => {
        return res.status(500).send({msg: "Error in Updating"});
      });
  } catch (err) {
    console.log(err.message);
    res.status(500).send({msg: "Error in Updating"});
  }
};

const destroy = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  const { id } = req.params;
  const { user } = req;
  try {
    Email.remove({
      _id: id,
      user: user._id,
    })
      .then((data) => {
        res.status(200).json({ success: true });
      })
      .catch((er) => {
        res.status(500).send({msg: "Error in Deleting"});
      });
  } catch (err) {
    console.log(err.message);
    res.status(500).send({msg: "Error in Deleting"});
  }
};

module.exports = {
  create,
  get,
  getAll,
  update,
  destroy,
};
