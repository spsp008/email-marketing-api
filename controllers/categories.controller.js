const { validationResult } = require("express-validator");
const Category = require("../model/Category");
const Email = require("../model/Email");

const create = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  const { name } = req.body;
  try {
    let category = await Category.findOne({
      name,
    });
    if (category) {
      return res.status(400).json({
        msg: "Category Already Exists",
      });
    }

    category = new Category({
      name,
    });

    await category
      .save()
      .then((data) => {
        const payload = {
          category: {
            name: category.name,
          },
          success: true
        };
        res.status(200).json(payload);
      })
      .catch((er) => {
        res.status(500).send({msg: "Error in Saving"});
      });
  } catch (err) {
    console.log(err.message);
    res.status(500).send({msg: "Error in Saving"});
  }
};

const get = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  const { id } = req.params;
  try {
    let category = await Category.findOne({
      _id: id,
    });
    if (!category) {
      return res.status(404).json({
        msg: "Category Not Exists",
      });
    }

    const payload = {
      category,
      success: true
    };
    res.status(200).json(payload);
  } catch (err) {
    console.log(err.message);
    res.status(500).send({msg: "Error in Fetching"});
  }
};

const getAll = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  try {
    let categories = await Category.find();
    if (!categories) {
      return res.status(404).json({
        msg: "Category Not Exists",
      });
    }

    const payload = {
      categories,
      success: true
    };
    res.status(200).json(payload);
  } catch (err) {
    console.log(err.message);
    res.status(500).send({msg: "Error in Fetching"});
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  const { id } = req.params;
  const { body } = req;
  const { name } = body
  try {
    Category.updateOne(
      {
        _id: id,
      },
      { name }
    )
      .then((data) => {
        return res.status(200).json({ success: true });
      })
      .catch((er) => {
        return res.status(500).send({msg: "Error in Updating"});
      });
  } catch (err) {
    console.log(err.message);
    res.status(500).send({msg: "Error in Updating"});
  }
};

const destroy = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  const { id } = req.params;
  const { user } = req;
  try {
    Category.remove({
      _id: id,
    })
      .then((data) => {
        Email.remove({
          user: user._id,
          category: id,
        })
          .then((data) => {
            return res.status(200).json({ success: true });
          })
          .catch((er) => {
            res.status(500).send({msg: "Error in Deleting"});
          });
      })
      .catch((er) => {
        res.status(500).send({msg: "Error in Deleting"});
      });
  } catch (err) {
    console.log(err.message);
    res.status(500).send({msg: "Error in Deleting"});
  }
};

module.exports = {
  create,
  get,
  getAll,
  destroy,
  update,
};
