const { validationResult } = require("express-validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const User = require("../model/User");

const signup = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  const { username, email, password } = req.body;
  try {
    let user = await User.findOne({
      email,
    });
    if (user) {
      return res.status(400).json({
        msg: "User Already Exists",
      });
    }

    user = new User({
      username,
      email,
      password,
    });

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);

    await user.save();
    res.status(201).json({success: true});

  } catch (err) {
    console.log(err.message);
    res.status(500).send("Error in Saving");
  }
};

const login =   async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array()
    });
  }

  const { email, password } = req.body;
  try {
    let user = await User.findOne({
      email
    });
    if (!user)
      return res.status(400).json({
        message: 'User Not Exist'
      });

    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch)
      return res.status(400).json({
        message: 'Incorrect Password!'
      });

    const payload = {
      user: {
        _id: user._id,
        username: user.username,
        email: user.email
      }
    };

    jwt.sign(
      payload,
      'randomString',
      {
        expiresIn: 3600 * 24
      },
      (err, token) => {
        if (err) throw err;
        payload.user.token = token;
        payload.success = true;
        res.status(200).json(
          payload
        );
      }
    );
  } catch (e) {
    console.error(e);
    res.status(500).json({
      message: 'Server Error'
    });
  }
};

module.exports = {
  signup,
  login
}
