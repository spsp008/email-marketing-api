const express = require('express');
const { check } = require('express-validator');
const router = express.Router();
const auth = require('../middleware/auth');
const {signup, login} = require('../controllers/users.controller.js');

router.post(
  '/signup',
  [
    check('username', 'Please Enter a Valid Username')
    .not()
    .isEmpty(),
    check('email', 'Please enter a valid email').isEmail(),
    check('password', 'Please enter a valid password').isLength({
        min: 8
    })
  ],
  signup
);

router.post(
  '/login',
  [
    check('email', 'Please enter a valid email').isEmail(),
    check('password', 'Please enter a valid password').isLength({
      min: 6
    })
  ],
  login
);

module.exports = router;
