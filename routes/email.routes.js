const express = require('express');
const { check } = require('express-validator');
const router = express.Router();
const auth = require('../middleware/auth');
const { create, get, getAll, update, destroy } = require('../controllers/emails.controllers')
router.post(
  '/',
  [
    auth,
    check('emailId', 'Please Enter a emailId')
    .isEmail(),
  ],
  create
);

router.get(
  '/:id',
  [
    auth
  ],
  get
);

router.get(
  '/',
  [
    auth
  ],
  getAll
);

router.delete(
  '/:id',
  [
    auth
  ],
  destroy
);

router.put(
  '/:id',
  [
    auth
  ],
  update
);

module.exports = router;
