const express = require('express');
const { check } = require('express-validator');
const router = express.Router();
const auth = require('../middleware/auth');
const { create, get, getAll, update, destroy } = require('../controllers/categories.controller.js');

router.post(
  '/',
  [
    auth,
    check('name', 'Please Enter a name')
    .not()
    .isEmpty(),
  ],
  create
);

router.get(
  '/:id',
  [
    auth
  ],
  get
);

router.get(
  '/',
  [
    auth
  ],
  getAll
);

router.delete(
  '/:id',
  [
    auth
  ],
  destroy
);

router.put(
  '/:id',
  [
    auth
  ],
  update
);

module.exports = router;
