const express = require('express');
const router = express.Router();
const dotenv = require('dotenv').config();

const userRoutes = require('./user.routes.js');
const categoryRoutes = require('./category.routes.js');
const emailRoutes = require('./email.routes.js');

router.get('/', (req, res) => {
  res.send('API Working in ' + process.env.NODE_ENV)
});

router.use('/user', userRoutes);
router.use('/category', categoryRoutes);
router.use('/email', emailRoutes);

module.exports = router;
