const express = require('express');
const app = express();
const cors = require('cors')
const InitiateMongoServer = require('./config/db');
const routes = require('./routes');

// Initiate Mongo Server
InitiateMongoServer();

// PORT
const PORT = process.env.PORT || 3000;

// Middleware
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use(cors());

app.use(routes);

app.listen(PORT, (req, res) => {
  console.log(`Server Started at PORT ${PORT}`);
});
