const mongoose = require('mongoose');
// const { Schema } = mongoose;

const EmailSchema = mongoose.Schema({
  emailId: {
    type: String,
    required: true,
    unique: true
  },
  category: { type: mongoose.Schema.Types.ObjectId, ref: 'category', required: true },
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'user', required: true }
},{
  timestamps: true
});

// export model user with UserSchema
module.exports = mongoose.model('email', EmailSchema);
